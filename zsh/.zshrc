source ~/.config/zsh/configs/prompt.zsh
source ~/.config/zsh/configs/options.zsh
source ~/.config/zsh/configs/vi-mode.zsh
source ~/.config/zsh/configs/aliases.zsh

source ~/.config/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ~/.config/zsh/plugins/zsh-autosuggestions.zsh
source ~/.config/zsh/plugins/zsh-extract.zsh
